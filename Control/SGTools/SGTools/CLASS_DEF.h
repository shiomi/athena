/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef SGTOOLS_CLASS_DEF_H
#define SGTOOLS_CLASS_DEF_H
/** @file CLASS_DEF.h
 *  @brief macros to associate a CLID to a type
 *
 *  @author Paolo Calafiura <pcalafiura@lbl.gov>
 *  $Id: CLASS_DEF.h,v 1.3 2009-01-15 19:07:29 binet Exp $
 */


#include "AthenaKernel/CLASS_DEF.h"


#endif // not SGTOOLS_CLASS_DEF_H
